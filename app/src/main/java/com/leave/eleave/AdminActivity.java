package com.leave.eleave;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.leave.eleave.model.Leave;
import com.leave.eleave.sql.LeaveHelper;

import java.util.ArrayList;

public class AdminActivity extends AppCompatActivity {

    LeaveHelper leaveHelper;
    ArrayList leaves;
    Context context;
    AdminAdapter adapter;

    RecyclerView rvPendingLeaves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        rvPendingLeaves = (RecyclerView) findViewById(R.id.pending_leave_list);
        context = this;

        leaveHelper = new LeaveHelper(this);
        leaves = leaveHelper.getPendingLeaves();

        // Populate the list (-___-)
        adapter = new AdminAdapter(this, leaves, new AdminAdapter.ApprovalListener() {
            @Override
            public void acceptOnClick(View v, int position) {
                Leave leave = (Leave) leaves.get(position);
                leaveHelper.approveLeave(leave.getLeaveId());
                Toast.makeText(context, "LeaveId: " + leave.getLeaveId() + " has been approved",
                        Toast.LENGTH_SHORT).show();
                adapter.removeItem(position);
            }

            @Override
            public void rejectOnClick(View v, int position) {
                Leave leave = (Leave) leaves.get(position);
                leaveHelper.rejectLeave(leave.getLeaveId());
                Toast.makeText(context, "LeaveId: " + leave.getLeaveId() + " has been rejected",
                        Toast.LENGTH_SHORT).show();
                adapter.removeItem(position);
            }
        });
        rvPendingLeaves.setAdapter(adapter);
        rvPendingLeaves.setLayoutManager(new LinearLayoutManager(this));

        Log.v("READ", "PENDING LEAVES DISPLAYED");
    }
}
