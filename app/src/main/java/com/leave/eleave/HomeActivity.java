package com.leave.eleave;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.leave.eleave.model.Employee;
import com.leave.eleave.model.Leave;
import com.leave.eleave.sql.EmployeeHelper;
import com.leave.eleave.sql.LeaveHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    EmployeeHelper employeeHelper;
    LeaveHelper leaveHelper;
    Leave leave;
    int empId;

    ApplyFragment tab1;
    BalanceFragment tab2;
    AccountFragment tab3;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), 3);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    /**
     * A placeholder fragment containing a simple view.

    public static class PlaceholderFragment extends Fragment {

         * The fragment argument representing the section number for this
         * fragment.

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


         * Returns a new instance of this fragment for the given section
         * number.

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_home, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }
    */

    // Above class got commented out because class below does it better

    // 3 fragments instead of above class I believe
    // use getItem() below to choose appropriate Fragment
    // Update: Yep yep it worked

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        int numTabs;

        public SectionsPagerAdapter(FragmentManager fm, int numTabs) {
            super(fm);
            this.numTabs = numTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    tab1 = new ApplyFragment();
                    return tab1;
                case 1:
                    tab2 = new BalanceFragment();
                    return tab2;
                case 2:
                    tab3 = new AccountFragment();
                    return tab3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return numTabs;
        }
    }

    // Apply leave button listener. Data goes into database from HERE and not from ApplyFragment
    public void applyLeave(View view) {
        Log.v("READ", "Leave called");
        String format = "dd/MM/yyyy";
        long startEpoch = -1;
        long endEpoch = -1;

        Intent intent = this.getIntent();
        empId = intent.getIntExtra(String.valueOf(R.string.logged_employee_id), -1);

        leaveHelper = new LeaveHelper(this);
        employeeHelper = new EmployeeHelper(this);
        
        String type = String.valueOf(tab1.reason.getSelectedItem());
        String from = tab1.start.getText().toString();
        String to   = tab1.end.getText().toString();

        try {
            startEpoch = new SimpleDateFormat(format).parse(from).getTime();
            endEpoch   = new SimpleDateFormat(format).parse(to).getTime();
            leave = new Leave(empId, type, startEpoch, endEpoch);
            leaveHelper.addLeave(leave);

            Log.v("READ", "Try worked");
            Log.v("READ", "Leave sent to database");
        } catch (ParseException e) {
            Log.v("Date error", e.toString());
        }

        // Subtract number of days from balance
        if (startEpoch != -1) {
            long days = ChronoUnit.DAYS.between(Instant.ofEpochMilli(startEpoch),
                    Instant.ofEpochMilli(endEpoch));
            Log.v("READ", "Number of days calculated");
            Employee emp = employeeHelper.getBalance(String.valueOf(empId));

            if(type.equals("Paid Leave")) {
                emp.setPaidLeaves((int) (emp.getPaidLeaves() - days));
            } else if(type.equals("Sick Leave")) {
                emp.setSickLeaves((int) (emp.getSickLeaves() - days));
            } else if(type.equals("Casual Leave")) {
                emp.setCasualLeaves((int) (emp.getCasualLeaves() - days));
            } else {
                emp.setEmergencyLeaves((int) (emp.getEmergencyLeaves() - days));
            }

            employeeHelper.updateEmployeeBalance(emp);
            Log.v("READ", "Balances updated");
        }

        // Refresh all fragments after data has been changed
        mSectionsPagerAdapter.notifyDataSetChanged();
        tab2.refresh();

        Toast.makeText(this, "Leave application has been sent:\n"
                + type + "\nFrom: " + from + "\nTo: " + to, Toast.LENGTH_SHORT).show();
        Log.v("READ", "Updated fragments");
    }
}
