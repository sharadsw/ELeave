package com.leave.eleave;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.leave.eleave.model.Leave;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.Instant;
import java.util.List;

/**
 * Created by user on 3/29/18.
 */

public class AdminAdapter extends RecyclerView.Adapter<AdminAdapter.ViewHolder> {

    List<Leave> leaves;
    Context mContext;
    ApprovalListener onClickListener;

    public AdminAdapter(Context context, List<Leave> leaves, ApprovalListener listener) {
        this.leaves = leaves;
        this.mContext = context;
        this.onClickListener = listener;
    }

    private Context getContext() {
        return mContext;
    }

    // Inflates the layout
    @Override
    public AdminAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the layout
        View empLeaveView = inflater.inflate(R.layout.pending_leaves, parent, false);

        // Return a new holder instance (Idk whats going on)
        ViewHolder viewHolder = new ViewHolder(empLeaveView);
        return  viewHolder;
    }

    // Sets the data
    @Override
    public void onBindViewHolder(AdminAdapter.ViewHolder holder, int position) {
        Leave leave = leaves.get(position);

        // Name actually holds Id
        TextView mName = holder.name;
        TextView mReason = holder.reason;
        TextView mFrom = holder.from;
        TextView mTo = holder.to;
        //TextView mApproved = holder.approved;

        mName.setText("Employee Id: " + leave.getEmpId());
        mReason.setText(leave.getType());
        mFrom.setText(convertEpoch(leave.getStartDate()) + " to");
        mTo.setText(convertEpoch(leave.getEndDate()));
        //mApproved.setText(convertApproved(leave.getApproved()));
    }

    // Gets the total amount of elements in the list
    @Override
    public int getItemCount() {
        return leaves.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView reason;
        TextView from;
        TextView to;
        //TextView approved;

        Button accept;
        Button reject;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.admin_empId);
            reason = (TextView) itemView.findViewById(R.id.admin_reason);
            from = (TextView) itemView.findViewById(R.id.admin_leaveFrom);
            to = (TextView) itemView.findViewById(R.id.admin_leaveTo);
            //approved = (TextView) itemView.findViewById(R.id.approvedStatus);

            accept = (Button) itemView.findViewById(R.id.approve_button);
            reject = (Button) itemView.findViewById(R.id.reject_button);

            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.acceptOnClick(v, getAdapterPosition());
                }
            });
            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.rejectOnClick(v, getAdapterPosition());
                }
            });
        }
    }

    public String convertEpoch(long epoch) {
        Date date = Date.from(Instant.ofEpochMilli(epoch));

        return new SimpleDateFormat("dd-MM-yyyy").format(date);
    }

    /*
    public String convertApproved(int approved) {
        switch (approved) {
            case -1: return "Rejected";
            case  0: return "Not Approved";
            case  1: return "Approved";
        }

        return null;
    }
    */

    // This class was copied off of EmployeeLeaveAdapter

    public interface ApprovalListener {

        void acceptOnClick(View v, int position);

        void rejectOnClick(View v, int position);
    }

    public void removeItem(int position) {
        leaves.remove(position);
        notifyItemRemoved(position);
    }
}
