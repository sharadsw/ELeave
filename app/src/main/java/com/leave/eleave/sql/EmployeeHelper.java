package com.leave.eleave.sql;

/**
 * Created by user on 3/25/18.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.leave.eleave.model.Employee;

import java.util.ArrayList;

public class EmployeeHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "EmployeeManager.db";

    // User table name
    private static final String TABLE_NAME = "employee";

    // User Table Columns names
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_UN = "username";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_PHONE = "phone";
    private static final String COLUMN_AGE = "age";

    private static final String COLUMN_SICK = "sick";
    private static final String COLUMN_CASUAL = "casual";
    private static final String COLUMN_PAID = "paid";
    private static final String COLUMN_EMG = "emg";

    private static final String COLUMN_ADMIN = "admin";

    // create table sql query
    private String CREATE_EMPLOYEE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_UN + " TEXT,"
            + COLUMN_NAME + " TEXT," + COLUMN_PASSWORD + " TEXT,"
            + COLUMN_PHONE + " INTEGER," + COLUMN_AGE + " INTEGER,"
            + COLUMN_SICK + " INTEGER," + COLUMN_CASUAL + " INTEGER,"
            + COLUMN_PAID + " INTEGER," + COLUMN_EMG + " INTEGER,"
            + COLUMN_ADMIN + " INTEGER DEFAULT 0" + ")";

    // drop table sql query
    private String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public EmployeeHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_EMPLOYEE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //Drop User Table if exist
        db.execSQL(DROP_TABLE);

        // Create tables again
        onCreate(db);
    }

    // Helper methods below

    public void addEmployee(Employee employee) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_UN, employee.getUsername());
        values.put(COLUMN_NAME, employee.getName());
        values.put(COLUMN_PASSWORD, employee.getPassword());
        values.put(COLUMN_PHONE, employee.getPhone());
        values.put(COLUMN_AGE, employee.getAge());

        values.put(COLUMN_SICK, employee.getSickLeaves());
        values.put(COLUMN_CASUAL, employee.getCasualLeaves());
        values.put(COLUMN_PAID, employee.getPaidLeaves());
        values.put(COLUMN_EMG, employee.getEmergencyLeaves());

        values.put(COLUMN_ADMIN, employee.getAdmin());

        // Insert the above values ^____^
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void updateEmployeeBalance(Employee employee) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_SICK, employee.getSickLeaves());
        values.put(COLUMN_CASUAL, employee.getCasualLeaves());
        values.put(COLUMN_PAID, employee.getPaidLeaves());
        values.put(COLUMN_EMG, employee.getEmergencyLeaves());

        // Update the above values \('_')/
        db.update(TABLE_NAME, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(employee.getId())});
        db.close();
    }

    public Employee loginEmployee(String username, String password) {
        Employee employee = new Employee();

        // array of columns to fetch
        String[] columns = {
                COLUMN_ID,
                COLUMN_ADMIN
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection columns
        String selection = COLUMN_UN + " = ?" + " AND " + COLUMN_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {username, password};

        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        if (cursor != null && cursorCount > 0) {
            cursor.moveToFirst();
            employee.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
            employee.setAdmin(cursor.getInt(cursor.getColumnIndex(COLUMN_ADMIN)));
            cursor.close();
        }

        db.close();

        return employee;
    }

    // FFS could just return an employee instance instead of a list
    // Could also avoid using String as an id :/
    public Employee getBalance(String empId) {
        Employee emp = new Employee();

        String[] columns = {
                COLUMN_PAID,
                COLUMN_SICK,
                COLUMN_CASUAL,
                COLUMN_EMG
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_ID + " = ?";

        // selection arguments for above criteria
        String[] selectionArgs = {empId};

        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        if (cursor != null && cursorCount > 0) {
            cursor.moveToFirst();
            emp.setPaidLeaves(cursor.getInt(cursor.getColumnIndex(COLUMN_PAID)));
            emp.setSickLeaves(cursor.getInt(cursor.getColumnIndex(COLUMN_SICK)));
            emp.setCasualLeaves(cursor.getInt(cursor.getColumnIndex(COLUMN_CASUAL)));
            emp.setEmergencyLeaves(cursor.getInt(cursor.getColumnIndex(COLUMN_EMG)));
            cursor.close();
        }

        emp.setId(Integer.parseInt(empId));

        db.close();

        return emp;
    }
}
