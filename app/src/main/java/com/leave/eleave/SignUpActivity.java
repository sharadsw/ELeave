package com.leave.eleave;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.leave.eleave.model.Employee;
import com.leave.eleave.sql.EmployeeHelper;

public class SignUpActivity extends AppCompatActivity {

    EditText password;
    EditText cPassword;

    EditText username;
    EditText name;
    EditText phone;
    EditText age;
    Spinner  type;

    private Employee employee;
    private EmployeeHelper employeeHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        password  = (EditText) findViewById(R.id.password);
        cPassword = (EditText) findViewById(R.id.confirm_password);

        username = (EditText) findViewById(R.id.username);
        name     = (EditText) findViewById(R.id.name);
        phone    = (EditText) findViewById(R.id.phone);
        age      = (EditText) findViewById(R.id.age);
        type     = (Spinner) findViewById(R.id.type_spinner);

        employee       = new Employee();
        employeeHelper = new EmployeeHelper(this);
    }

    public void submitData(View view) {

        if(password.getText().toString().equals(cPassword.getText().toString())) {

            Log.v("READ", "BUTTON WORKS");

            employee.setUsername(username.getText().toString().trim());
            employee.setName(name.getText().toString().trim());
            employee.setPassword(password.getText().toString().trim());
            employee.setAge(Integer.parseInt(age.getText().toString().trim()));
            employee.setPhone(Integer.parseInt(phone.getText().toString().trim()));

            if(type.getSelectedItem().toString().equals("Admin")) {
                employee.setAdmin();
            }

            employeeHelper.addEmployee(employee);

            Log.v("READ", "DATABASE WORKS");

            this.finish();
        }
        else {
            Toast.makeText(this, "Passwords do not match", Toast.LENGTH_SHORT).show();
        }
    }

    public void resetData(View view) {
        username.setText(null);
        name.setText(null);
        password.setText(null);
        cPassword.setText(null);
        phone.setText(null);
        age.setText(null);
    }
}
