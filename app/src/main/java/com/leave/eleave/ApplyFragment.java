package com.leave.eleave;

/**
 * Created by user on 3/24/18.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ApplyFragment extends Fragment {

    Spinner reason;
    EditText start;
    EditText end;

    View view;

    int empId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.apply_fragment, container, false);

        reason = (Spinner) view.findViewById(R.id.reason_spinner);
        start  = (EditText) view.findViewById(R.id.from_date);
        end    = (EditText) view.findViewById(R.id.to_date);

        Log.v("READ", "Finished initialization in apply fragment");
        return view;
    }
}