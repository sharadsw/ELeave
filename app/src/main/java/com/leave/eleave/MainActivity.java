package com.leave.eleave;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.leave.eleave.model.Employee;
import com.leave.eleave.sql.EmployeeHelper;

public class MainActivity extends AppCompatActivity {

    EditText username;
    EditText password;

    // String tempUser = "employee";
    // String tempPw   = "123456";

    EmployeeHelper employeeHelper;
    int empId;

    String ERROR_MSG = "Invalid Username or Password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        employeeHelper = new EmployeeHelper(this);
    }

    // Event function for the sign in button
    public void signIn(View view) {
        /*
        if(username.getText().toString().equals(tempUser) && password.getText().toString().equals(tempPw)) {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }
        else {
            Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
        }
        */
        // Have a good laugh at the bullshit above

        // Split this also into admin and employee with separate methods for both, IDEALLY

        Employee emp = employeeHelper.loginEmployee(username.getText().toString(), password.getText().toString());
        empId = emp.getId();

        if (empId != 0) {
            Log.v("READ", "AYYY LOGIN WORKS");

            if(emp.getAdmin() == 0) {
                Intent intent = new Intent(this, HomeActivity.class);
                intent.putExtra(String.valueOf(R.string.logged_employee_id), empId);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, AdminActivity.class);
                startActivity(intent);
            }
        } else {
            Toast.makeText(this, ERROR_MSG, Toast.LENGTH_SHORT).show();
        }
    }

    // Event function for the sign up button
    public void signUp(View view) {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }
}
