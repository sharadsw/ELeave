package com.leave.eleave.model;

/**
 * Created by user on 3/25/18.
 */

public class Employee {

    int DEFAULT_PAID   = 5;
    int DEFAULT_SICK   = 20;
    int DEFAULT_CASUAL = 10;
    int DEFAULT_EMG    = 10;

    private int id;
    private String username;
    private String name;
    private String password;
    private int phone;
    private int age;

    private int sickLeaves;
    private int casualLeaves;
    private int paidLeaves;
    private int emergencyLeaves;

    private int admin;

    public Employee() {
        this.sickLeaves = DEFAULT_SICK;
        this.casualLeaves = DEFAULT_CASUAL;
        this.paidLeaves = DEFAULT_PAID;
        this.emergencyLeaves = DEFAULT_EMG;
        this.admin = 0;
    }

    public Employee(String username, String name, String password, int phone, int age, int admin) {
        this.username = username;
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.age = age;
        this.sickLeaves = DEFAULT_SICK;
        this.casualLeaves = DEFAULT_CASUAL;
        this.paidLeaves = DEFAULT_PAID;
        this.emergencyLeaves = DEFAULT_EMG;
        this.admin = admin;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSickLeaves() {
        return sickLeaves;
    }

    public void setSickLeaves(int sickLeaves) {
        this.sickLeaves = sickLeaves;
    }

    public int getCasualLeaves() {
        return casualLeaves;
    }

    public void setCasualLeaves(int casualLeaves) {
        this.casualLeaves = casualLeaves;
    }

    public int getPaidLeaves() {
        return paidLeaves;
    }

    public void setPaidLeaves(int paidLeaves) {
        this.paidLeaves = paidLeaves;
    }

    public int getEmergencyLeaves() {
        return emergencyLeaves;
    }

    public void setEmergencyLeaves(int emergencyLeaves) {
        this.emergencyLeaves = emergencyLeaves;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin() {
        this.admin = 1;
    }

    public void setAdmin(int val) {
        this.admin = val;
    }
}
