package com.leave.eleave;

/**
 * Created by user on 3/24/18.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leave.eleave.model.Employee;
import com.leave.eleave.sql.EmployeeHelper;

import java.util.ArrayList;

public class BalanceFragment extends Fragment {

    TextView paid;
    TextView sick;
    TextView casual;
    TextView emergency;

    View view;
    Employee emp;
    Context context;

    int empId;

    private EmployeeHelper employeeHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.balance_fragment, container, false);

        paid      = (TextView) view.findViewById(R.id.pleave);
        sick      = (TextView) view.findViewById(R.id.sleave);
        casual    = (TextView) view.findViewById(R.id.cleave);
        emergency = (TextView) view.findViewById(R.id.eleave);

        Log.v("READ", "Finished initialization in balance fragment");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.v("READ", "Updating balance values");

        Activity parent = this.getActivity();
        Intent intent = parent.getIntent();
        empId = intent.getIntExtra(String.valueOf(R.string.logged_employee_id), -1);

        if(empId != -1) {
            Log.v("READ", "Successfully found balance values");

            context = this.getContext();
            employeeHelper = new EmployeeHelper(context);
            emp = employeeHelper.getBalance(String.valueOf(empId));

            paid.setText(null);
            sick.setText(null);
            casual.setText(null);
            emergency.setText(null);

            paid.setText(Integer.toString(emp.getPaidLeaves()));
            sick.setText(Integer.toString(emp.getSickLeaves()));
            casual.setText(Integer.toString(emp.getCasualLeaves()));
            emergency.setText(Integer.toString(emp.getEmergencyLeaves()));
        }
    }

    public void refresh() {
        emp = employeeHelper.getBalance(String.valueOf(empId));

        paid.setText(null);
        sick.setText(null);
        casual.setText(null);
        emergency.setText(null);

        paid.setText(Integer.toString(emp.getPaidLeaves()));
        sick.setText(Integer.toString(emp.getSickLeaves()));
        casual.setText(Integer.toString(emp.getCasualLeaves()));
        emergency.setText(Integer.toString(emp.getEmergencyLeaves()));
    }
}