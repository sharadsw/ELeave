package com.leave.eleave;

/**
 * Created by user on 3/24/18.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leave.eleave.sql.LeaveHelper;

import java.util.List;

public class AccountFragment extends Fragment {

    RecyclerView rvLeaves;
    View view;
    Context context;

    LeaveHelper leaveHelper;
    List leaves;
    int empId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view = inflater.inflate(R.layout.account_fragment, container, false);

        rvLeaves = (RecyclerView) view.findViewById(R.id.leave_history_list);

        Log.v("READ", "CREATED ACCOUNT FRAGMENT");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.v("READ", "Creating leave list");

        Activity parent = this.getActivity();
        Intent intent = parent.getIntent();
        empId = intent.getIntExtra(String.valueOf(R.string.logged_employee_id), -1);

        if(empId != -1) {
            Log.v("READ", "FILLING THE LIST");

            context = this.getContext();
            leaveHelper = new LeaveHelper(context);

            leaves = leaveHelper.getLeaves(String.valueOf(empId));

            // Populate the list
            EmployeeLeaveAdapter adapter = new EmployeeLeaveAdapter(context, leaves);
            rvLeaves.setAdapter(adapter);
            rvLeaves.setLayoutManager(new LinearLayoutManager(context));

            Log.v("READ", "LIST SHOULD BE WORKING");
        }
    }
}