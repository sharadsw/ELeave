package com.leave.eleave.model;

/**
 * Created by user on 3/25/18.
 */

public class Leave {

    private int leaveId;
    private int empId;
    private String type;
    private long startDate;
    private long endDate;
    private int approved;

    public Leave() {
        approved = 0;
    }

    public Leave(int leaveId, int empId, String type, long startDate, long endDate) {
        this.leaveId = leaveId;
        this.empId = empId;
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.approved = 0;
    }

    public Leave(int leaveId, int empId, String type, long startDate, long endDate, int approved) {
        this.leaveId = leaveId;
        this.empId = empId;
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.approved = approved;
    }

    public Leave(int empId, String type, long startDate, long endDate) {
        this.empId = empId;
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;
        this.approved = 0;
    }

    public int getEmpId() {

        return empId;
    }

    public int getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(int leaveId) {
        this.leaveId = leaveId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved() {
        this.approved = 1;
    }

    public void reject() {
        this.approved = -1;
    }
}
