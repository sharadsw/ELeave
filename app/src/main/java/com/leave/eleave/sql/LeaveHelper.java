package com.leave.eleave.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.leave.eleave.model.Leave;

import java.util.ArrayList;

/**
 * Created by user on 3/26/18.
 */

public class LeaveHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "LeaveManager.db";

    // User table name
    private static final String TABLE_NAME = "leaves";

    // User Table Columns names
    private static final String COLUMN_ID = "l_id";
    private static final String COLUMN_EMP_ID = "emp_id";
    private static final String COLUMN_TYPE = "type";
    private static final String COLUMN_START_DATE = "start";
    private static final String COLUMN_END_DATE = "end";
    private static final String COLUMN_APPROVED = "approved";

    // create table sql query
    private String CREATE_LEAVE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_EMP_ID + " INTEGER,"
            + COLUMN_TYPE + " TEXT," + COLUMN_START_DATE + " INTEGER," 
            + COLUMN_END_DATE + " INTEGER," + COLUMN_APPROVED + " INTEGER" + ")";

    // drop table sql query
    private String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public LeaveHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_LEAVE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        //Drop User Table if exist
        db.execSQL(DROP_TABLE);

        // Create tables again
        onCreate(db);
    }

    public void addLeave(Leave leave) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_EMP_ID, leave.getEmpId());
        values.put(COLUMN_TYPE, leave.getType());
        values.put(COLUMN_START_DATE, leave.getStartDate());
        values.put(COLUMN_END_DATE, leave.getEndDate());
        values.put(COLUMN_APPROVED, leave.getApproved());

        // Insert the above values (*____*)
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void approveLeave(int leaveId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_APPROVED, 1);

        // Update the above values \(*_o)/
        db.update(TABLE_NAME, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(leaveId)});
        db.close();
    }

    public void rejectLeave(int leaveId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_APPROVED, -1);

        // Update the above values \(*_o)/
        db.update(TABLE_NAME, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(leaveId)});
        db.close();
    }

    public ArrayList getLeaves(String empId) {
        ArrayList<Leave> leaves = new ArrayList<Leave>();

        // Columns to be returned
        String[] columns = {
                COLUMN_ID,
                COLUMN_TYPE,
                COLUMN_START_DATE,
                COLUMN_END_DATE,
                COLUMN_APPROVED
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_EMP_ID + " = ?";

        // selection arguments for above criteria
        String[] selectionArgs = { empId };

        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        if (cursor != null && cursorCount > 0) {
            cursor.moveToFirst();

            while(!cursor.isAfterLast()) {

                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
                String type = cursor.getString(cursor.getColumnIndex(COLUMN_TYPE));
                long start = cursor.getLong(cursor.getColumnIndex(COLUMN_START_DATE));
                long end = cursor.getLong(cursor.getColumnIndex(COLUMN_END_DATE));
                int app = cursor.getInt(cursor.getColumnIndex(COLUMN_APPROVED));

                Leave temp = new Leave(id, Integer.parseInt(empId), type, start, end, app);
                leaves.add(temp);

                cursor.moveToNext();
            }

            cursor.close();
        }

        db.close();

        return leaves;
    }

    public ArrayList<Leave> getPendingLeaves() {
        ArrayList<Leave> leaves = new ArrayList<Leave>();

        // Columns to be returned
        String[] columns = {
                COLUMN_ID,
                COLUMN_EMP_ID,
                COLUMN_TYPE,
                COLUMN_START_DATE,
                COLUMN_END_DATE,
        };

        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_APPROVED + " = 0";

        // selection arguments for above criteria
        //String[] selectionArgs = { empId };

        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                null,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        if (cursor != null && cursorCount > 0) {
            cursor.moveToFirst();

            while(!cursor.isAfterLast()) {

                int l_id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
                int id = cursor.getInt(cursor.getColumnIndex(COLUMN_EMP_ID));
                String type = cursor.getString(cursor.getColumnIndex(COLUMN_TYPE));
                long start = cursor.getLong(cursor.getColumnIndex(COLUMN_START_DATE));
                long end = cursor.getLong(cursor.getColumnIndex(COLUMN_END_DATE));

                Leave temp = new Leave(id, type, start, end);
                temp.setLeaveId(l_id);
                leaves.add(temp);

                cursor.moveToNext();
            }

            cursor.close();
        }

        db.close();

        return leaves;
    }
}
